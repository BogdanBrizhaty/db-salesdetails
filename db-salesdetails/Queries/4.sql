﻿--4)	Який офіс (з якого міста) дає компанії найбільші прибутки?
-- Lomey

use [SalesOrders];
go

select max(CityProfit.Profit)
from (
	select EmpCity as City, sum(QuotedPrice * QuantityOrdered) as Profit
	from SalesOrders.dbo.Employees
		join Orders on Employees.EmployeeID = Orders.EmployeeID 
		join Order_Details on Orders.OrderNumber = Order_Details.OrderNumber
	group by EmpCity) as CityProfit
go

-- Full query
select EmpCity as City, sum(QuotedPrice * QuantityOrdered) as Profit
from SalesOrders.dbo.Employees
	join Orders on Employees.EmployeeID = Orders.EmployeeID 
	join Order_Details on Orders.OrderNumber = Order_Details.OrderNumber
group by EmpCity
having sum(QuotedPrice * QuantityOrdered) = 
(
	select max(CityProfit.Profit)
	from 
	(
		select EmpCity as City, sum(QuotedPrice * QuantityOrdered) as Profit
		from SalesOrders.dbo.Employees
			join Orders on Employees.EmployeeID = Orders.EmployeeID 
			join Order_Details on Orders.OrderNumber = Order_Details.OrderNumber
		group by EmpCity
	) as CityProfit
);