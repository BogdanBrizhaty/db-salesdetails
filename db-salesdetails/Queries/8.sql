﻿--8)	Визначте товар, від продажу одиниці якого чистий прибуток фірми є найбільшим.

use SalesOrders;

--go

--SELECT TOP 1 Products.ProductNumber, ProductName, QuantityOnHand, ProductDescription, QuotedPrice - WholesalePrice AS [Proceeds]
--FROM Products JOIN Order_Details
--ON Products.ProductNumber = Order_Details.ProductNumber
--JOIN Product_Vendors
--ON Products.ProductNumber = Product_Vendors.ProductNumber
--ORDER BY [Proceeds] DESC

go

declare @max money; 
select @max = max(Order_Details.QuotedPrice - Product_Vendors.WholesalePrice) from Product_Vendors
JOIN Order_Details
ON Product_Vendors.ProductNumber = Order_Details.ProductNumber

select distinct Products.ProductNumber, ProductName, QuantityOnHand, ProductDescription, QuotedPrice - WholesalePrice AS [Proceeds]
FROM Products JOIN Order_Details
ON Products.ProductNumber = Order_Details.ProductNumber
JOIN Product_Vendors
ON Products.ProductNumber = Product_Vendors.ProductNumber
where QuotedPrice - WholesalePrice = @max;