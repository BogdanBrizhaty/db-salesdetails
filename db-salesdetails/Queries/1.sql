﻿--1)	Знайдіть середню кількість одиниць товару в кожному замовлені.

-- Брижатый

use SalesOrders;

go

--avg(cast(Order_Details.QuantityOrdered as float))

select Order_Details.OrderNumber, avg(Order_Details.QuantityOrdered) as Average from Order_Details
	group by Order_Details.OrderNumber;