﻿--3)	Клієнтів з якого міста в компанії найбільше?

-- Vynohradov


use [SalesOrders];

go

	select  CustCity, count(CustCity) as perCity  from Customers 
	group by CustCity
	having COUNT(CustCity)=(
	select max (perCity1) from(
	select COUNT (CustCity) as perCity1 from Customers
	group by CustCity
	) as A
	)