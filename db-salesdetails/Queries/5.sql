﻿--5)	Який продукт з асортименту товарів купують найбільше?
-- Lomey

use [SalesOrders];
go


select Products.ProductName, sum(Order_Details.QuantityOrdered) as Quantity
from Order_Details
	join Products on Order_Details.ProductNumber = Products.ProductNumber
group by Products.ProductName
having sum(Order_Details.QuantityOrdered) = 
(
	select max(ProductsOrdered.Quantity)
	from 
	(
		select Products.ProductName, sum(Order_Details.QuantityOrdered) as Quantity
		from Order_Details
		join Products on Order_Details.ProductNumber = Products.ProductNumber
		group by Products.ProductName
	) as ProductsOrdered
);
go