﻿--2)	Яка сума найбільшого замовлення?
-- QuotedPrice или RetailPrice?
-- если исходить из найбільший == найбільша кількість товарів
-- Брижатый
use SalesOrders;

go

	declare @ORDERED_MAX int;

	select @ORDERED_MAX = max(sq.Ordered) from 
		(
			select Order_Details.OrderNumber as OrderId, sum(Order_Details.QuantityOrdered) as ordered from Order_Details
			group by Order_Details.OrderNumber
		) as sq;

	print @ORDERED_MAX;

	with theBiggestOrders (OrderId, QuantityOrdered, OrderCost)
	as
	(
				select Order_Details.OrderNumber as OrderId, sum(Order_Details.QuantityOrdered) as QuantityOrdered, sum(Order_Details.QuotedPrice) as OrderCost from Order_Details
				group by Order_Details.OrderNumber
	)
	select theBiggestOrders.OrderId, theBiggestOrders.QuantityOrdered, OrderCost from theBiggestOrders
	group by theBiggestOrders.OrderId, theBiggestOrders.QuantityOrdered, OrderCost
	having theBiggestOrders.QuantityOrdered = @ORDERED_MAX;




--- 2345 6 8


--select Order_Details.OrderNumber, Orders.OrderDate, sum(Order_Details.QuotedPrice) as "Сумма замовлення" from Order_Details
--join Orders on Orders.OrderNumber = Order_Details.OrderNumber
--group by Order_Details.OrderNumber, Orders.OrderDate
--having sum(Order_Details.QuotedPrice) =
--(select max(maxInfo.[sum]) from Order_Details
--	join 
--	(
--		select OrderNumber, sum(Order_Details.QuotedPrice) as [sum] from Order_Details
--		group by OrderNumber
--		) as maxInfo on maxInfo.OrderNumber = Order_Details.OrderNumber)