﻿--10)	Знайдіть всіх клієнтів, прізвище яких починається з літери «D» та які живуть в Портленді
-- Ломей
use SalesOrders;

go

select Customers.CustomerID, Customers.CustFirstName, Customers.CustLastName from Customers where CustLastName like 'D%';