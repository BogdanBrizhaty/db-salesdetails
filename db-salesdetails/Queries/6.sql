﻿--6)	Виведіть перелік замовлень, які доставлялись клієнтам довше 5 днів?
-- Lomey

use [SalesOrders];
go

-- Було
--SELECT DISTINCT Orders.OrderNumber, OrderDate, ShipDate, DaysToDeliver
--FROM Orders JOIN Order_Details
--ON Orders.OrderNumber = Order_Details.OrderNumber
--JOIN Products 
--ON Order_Details.ProductNumber = Products.ProductNumber
--JOIN Product_Vendors
--ON Products.ProductNumber = Product_Vendors.ProductNumber
--where DATEDIFF(dd, OrderDate, ShipDate) >= 4 --> 5

-- я переробив
select distinct Order_Details.OrderNumber, OrderDate, ShipDate, DATEDIFF(dd, OrderDate, ShipDate) as DaysToDeliver from Orders
JOIN Order_Details
ON Orders.OrderNumber = Order_Details.OrderNumber
JOIN Products 
ON Order_Details.ProductNumber = Products.ProductNumber
JOIN Product_Vendors
ON Products.ProductNumber = Product_Vendors.ProductNumber
where DATEDIFF(dd, OrderDate, ShipDate) >= 4
