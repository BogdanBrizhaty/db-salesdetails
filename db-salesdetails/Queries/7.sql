﻿--7)	Визначте товари, яких немає в наявності. Які з цих товарів можуть бути доставлені на склад за 4 дні?

use SalesOrders;

go
DECLARE @Count int = 120;

SELECT DISTINCT Products.ProductNumber, ProductName, QuantityOnHand,'delivery will not be' AS [Delivery]
FROM Products 
WHERE
QuantityOnHand < @Count AND ProductNumber IN
(
SELECT ProductNumber 
FROM Product_Vendors
GROUP BY ProductNumber
HAVING ProductNumber NOT IN 
(
SELECT ProductNumber 
FROM Product_Vendors 
WHERE DaysToDeliver = 4
)
)

UNION

SELECT DISTINCT Products.ProductNumber, ProductName, QuantityOnHand, 'delivery will be 4 days' AS [Delivery]
FROM Products JOIN Product_Vendors
ON Products.ProductNumber = Product_Vendors.ProductNumber
AND QuantityOnHand < @Count AND DaysToDeliver = 4
