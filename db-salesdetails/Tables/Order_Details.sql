﻿use SalesOrders;

go

drop table if exists Order_Details;

go

create table Order_Details
(
	OrderNumber int not null,
	ProductNumber int not null,
	QuotedPrice money default 0,
	QuantityOrdered int default 1,
	primary key (OrderNumber, ProductNumber)
)