﻿use SalesOrders;

go

drop table if exists Orders;

go

create table Orders
(
	OrderNumber int not null primary key,
	OrderDate date null default GETDATE(),
	ShipDate date not null,
	CustomerID int not null,
	EmployeeID int null
)
