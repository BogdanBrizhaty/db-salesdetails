﻿use SalesOrders;

go

drop table if exists Categories;

go

create table Categories
(
	CategoryID int not null primary key,
	CategoryDescription nvarchar(500) default 'No description provided yet'
)