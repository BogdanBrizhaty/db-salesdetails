﻿use SalesOrders;

go

drop table if exists Customers;

create table Customers
(
	CustomerID int not null primary key,
	CustFirstName nvarchar(25) null,
	CustLastName nvarchar(25) null,
	CustStreetAddress nvarchar(30) null,
	CustCity nvarchar(25) null,
	CustState nvarchar(25) null,
	CustZipCode nvarchar(10) null,
	CustAreCode int null,
	CustPhoneNumber nvarchar(25) null,
)