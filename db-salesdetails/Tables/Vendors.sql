﻿use SalesOrders;

go

drop table if exists Vendors;

create table Vendors
(
	VendorID int not null primary key,
	VendName nvarchar(25) null,
	VendStreetAddress nvarchar(30) null,
	VendCity nvarchar(25) null,
	VendState nvarchar(25) null,
	VendZipCode nvarchar(10) null,
	VendPhoneNumber nvarchar(25) null,
	VendFaxNumber nvarchar(25) null,
	VendWebPage nvarchar(50) null,
	VendEmailAddress nvarchar(50) null
)