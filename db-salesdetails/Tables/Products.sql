﻿use SalesOrders;

go

drop table if exists Products;

go

create table Products
(
	ProductNumber int not null primary key,
	ProductName nvarchar(50) null,
	ProductDescription nvarchar(2500) null default 'No description for this product available',
	RetailPrice money null default 0,
	QuantityOnHand int null default 1,
	CategoryID int not null
)