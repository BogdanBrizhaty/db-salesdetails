﻿use SalesOrders;

go

drop table if exists Employees;

create table Employees
(
	EmployeeID int not null primary key,
	EmpFirstName nvarchar(25) null,
	EmpLastName nvarchar(25) null,
	EmpStreetAddress nvarchar(30) null,
	EmpCity nvarchar(25) null,
	EmpState nvarchar(25) null,
	EmpZipCode nvarchar(10) null,
	EmpAreCode int null,
	EmpPhoneNumber nvarchar(25) null,
)