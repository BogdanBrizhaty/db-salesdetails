﻿use SalesOrders;

go

drop table if exists Product_Vendors;

go

create table Product_Vendors
(
	ProductNumber int not null,
	VendorID int not null,
	WholesalePrice money null default 0,
	DaysToDeliver int null,
	primary key(ProductNumber, VendorID)
)