﻿use SalesOrders;

go

alter table Orders
	add constraint [FK.Order_Customer_Reference] foreign key (CustomerID) references Customers(CustomerID) on delete cascade;

go

alter table Orders
	add constraint [FK.Order_Employee_Reference] foreign key (EmployeeID) references Employees(EmployeeID) on delete cascade;

go

alter table Order_Details
	add constraint [FK.OrderDetails_Order_Reference] foreign key (OrderNumber) references Orders(OrderNumber) on delete cascade;

go

alter table Order_Details
	add constraint [FK.OrderDetails_Product_Reference] foreign key (ProductNumber) references Products(ProductNumber) on delete cascade;

go

alter table Products
	add constraint [FK.Products_Categories_Reference] foreign key (CategoryID) references Categories(CategoryID) on delete cascade;

go

alter table Product_Vendors
	add constraint [FK.ProductVendors_Products_Reference] foreign key (ProductNumber) references Products(ProductNumber) on delete cascade;

go

alter table Product_Vendors
	add constraint [FK.ProductVendors_Vendors_Reference] foreign key (VendorID) references Vendors(VendorID) on delete cascade;