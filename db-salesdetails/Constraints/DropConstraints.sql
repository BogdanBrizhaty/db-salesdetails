﻿use SalesOrders;

go

alter table Orders
	drop constraint [FK.Order_Customer_Reference];

go

alter table Orders
	drop constraint [FK.Order_Employee_Reference];

go

alter table Order_Details
	drop constraint [FK.OrderDetails_Order_Reference];

go

alter table Order_Details
	drop constraint [FK.OrderDetails_Product_Reference];

go

alter table Products
	drop constraint [FK.Products_Categories_Reference];

go

alter table Product_Vendors
	drop constraint [FK.ProductVendors_Products_Reference];

go

alter table Product_Vendors
	drop constraint [FK.ProductVendors_Vendors_Reference];